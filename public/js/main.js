$(document).ready(function() {

    // jQuery leave my parameter names alone!
    $.ajaxSettings.traditional = true;

    var showroomId = $(".showroom").data("id"),
        favoritesCount = 0,
        favoritesArray = [];

    /*
     * Simple key/value store for tracking favorites. The keys are tied to the
     * showroom ID and work ID, to make them unique across all showrooms, and
     * removed favorites are simply deleted to keep the size of the store to
     * a minimum. This is more efficient that constructing some kind of JSON
     * object that has to be re-/de-serialized, or making Ajax requests to store
     * it in the session.
     *
     */

    function itemKey(workId) {
        return "showroom-favorites-"+showroomId+"-"+workId;
    }

    function addFavorite(workId) {
        window.localStorage.setItem(itemKey(workId), "true");
        updateFavorites();
        Piwik_.trackEvent('favs', 'add');
    }

    function delFavorite(workId) {
        window.localStorage.removeItem(itemKey(workId));
        updateFavorites();
        Piwik_.trackEvent('favs', 'remove');
    }

    function workIsFavorite(workId) {
        return window.localStorage.getItem(itemKey(workId)) === "true";
    }

    // Handles update of page state depending on the current state of the 
    // favorites list for this showroom.
    function updateFavorites() {
        var count = 0,
            $fcount = $("#favorites-count"),
            $fsend = $(".send-favorites");
        // We have to iterate all localStorage keys to find the ones we want.
        // I am 99% sure this is domain-based, so we are only going to iterate
        // keys for the works on this page, otherwise we risk having to iterate
        // keys stored for other showrooms.
        //
        // But since we are iterating this list anyway, we can go ahead and
        // ensure that the class assignments are all consistent as we do (and
        // indeed set it on page load, or on works that have just been marked).
        $(".work-item").map(function(idx, el) {
            var $el = $(el),
                id = $el.data("id");
            if (window.localStorage.getItem(itemKey(id)) === "true") {
                count++;
                $el.addClass("is-favorite");
                if (favoritesArray.indexOf(id) < 0) {
                    favoritesArray.push(id);
                }
            } else {
                $el.removeClass("is-favorite");
                favoritesArray = favoritesArray.filter(function(x) {
                    return x != id;
                });
            }
        });
        if (count) {
            $fcount.text(count.toString());
            $fsend.show();
        } else {
            $fcount.empty();
            $fsend.hide();
        }
        favoritesCount = count;
    }

    function filterList(filterStr, isFavorite) {
        var $visible = $(),
            $hidden = $(),
            $noWorks = $(".no-works-msg");

        $(".work-item").map(function(idx, el) {
        
            var $el = $(el),
                info = $el.find("a.lightbox").data("caption-info") || "",
                searchVal = true,
                favoriteVal = true;

            if (filterStr !== undefined) {
                // Search by filter
                searchVal = info.toLowerCase().indexOf(filterStr.toLowerCase()) > -1;
            }

            if (isFavorite !== undefined) {
                favoriteVal = workIsFavorite($el.data("id"));
            }
        
            if (favoriteVal && searchVal) {
                $visible = $visible.add($el);
            } else {
                $hidden = $hidden.add($el);
            }
        
        });
        
        $visible.show();
        $hidden.hide();

        if ($visible.length == 0) {
            $noWorks.show();
        } else {
            $noWorks.hide();
        }

    }

    var $filter = $('input[name="filter"]').on("keyup", function(e) {
        filterList($(this).val());
    }).on("keydown", function(e) {
        if (e.keyCode == 13) {
            $(this).blur();
        }
    }).trigger("keyup"); // in case of a lingering search value

    $("form#search").on("submit", function(e) {
        e.stopImmediatePropagation();
        e.preventDefault();
    });

    $('.show-favorites').on("click", function(e) {
        if (favoritesCount) {
            // Piwik_.trackEvent('favs', 'list');
            $(".nav .link").removeClass("active");
            $(e.target).addClass("active");
            $filter.val("");
            filterList(undefined, true);
        } else {
            bootbox.dialog({
                title: "No favorites selected",
                message: "You must select at least one favorite before you can filter by favorites.",
                buttons: {
                    ok: {
                        label: "Ok",
                        className: "btn-primary",
                        callback: function() {}
                    }
                }
            });
        }
    });

    // Bind to the body el, because the links inside the lightbox don't exist
    // unless the lightbox is active.
    $("body").on("click", ".add-favorite", function(e) {
        debugger;
        var id = $(e.target).data("id");
        addFavorite(id);
        e.preventDefault();
        e.stopImmediatePropagation();
    }).on("click", '.remove-favorite', function(e) {
        var id = $(e.target).data("id");
        delFavorite(id);
        e.preventDefault();
        e.stopImmediatePropagation();
    }).on("loadend.imagelightbox", function(e) {
        updateFavorites();
    }).on("loadstart.imagelightbox", function(e) {
        $("#imagelightbox-modal").removeClass("is-favorite");
    });

    $('.show-all').on("click", function(e) {
        $(".nav .link").removeClass("active");
        $(e.target).addClass("active");
        $filter.val("");
        filterList(undefined, undefined);
    });

    $('.send-favorites').on("click", function(e) {
        bootbox.dialog({
            title: "Send favorites",
            message: [
                "We're looking forward to your feedback!<br><br>Please provide a valid email address so that we can contact you. A copy your message will also be sent to the address you provide.",
                $("#send-favorites-form-template").html()
            ].join(""),
            buttons: {
                cancel: {
                    label: "Cancel",
                    callback: function() {}
                },
                send: {
                    label: "Send",
                    className: "btn-primary",
                    callback: function() {
                        var exportURL = window.location.pathname + "export/",
                            values = $("#send-favorites-form").serializeArray(),
                            payload = {
                                favorites: favoritesArray,
                            };
                        $.each(values, function(id, v) {
                            payload[v.name] = v.value;
                        });
                        Piwik_.trackEvent('favs', 'send');
                        $.ajax({
                            url: exportURL,
                            type: 'POST',
                            data: JSON.stringify(payload),
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                            success: function() {
                                Piwik_.trackEvent('favs', 'sent');
                                bootbox.dialog({
                                    title: "Favorites sent",
                                    message: "Your favorite selection was successfully sent.",
                                    buttons: {
                                        ok: {
                                            label: "Ok",
                                            className: "btn-primary",
                                            callback: function() {}
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    });

    // Finally, on window init, run these:

    updateFavorites();
});
