//leaving $(document).ready for this small % of browsers that do not accept it
$(document).ready(function() {
	
	var allDescriptionLinks = $('.works article .toggle');
	
	
	//on window resize number of elements in a row may change
	//so let's close the description if it does

	function countItemsInRow () {
		var firstOffset = allDescriptionLinks.offset().top;
		var itemsCount = 0;
		
		$.each(allDescriptionLinks, function(i, link){
			itemsCount = i;
			return $(link).offset().top === firstOffset ? true : false;
		});

		return itemsCount ;
	};
	
	//count initial number of columns in a row
	var itemsInRow = countItemsInRow();

	var onWindowResize = (function(itemsInRow, e){
		
		if (itemsInRow === countItemsInRow()) {
			return;
		}

		var closeDescription = function (el) {
			$(el).next('.description').hide('slow');
			$(el).removeClass('open');
		};

		$.each(allDescriptionLinks, function(i, link){
			closeDescription(link);
		});
	}).bind(this, itemsInRow);

	$(window).on('resize', onWindowResize);

	//open description for clicked link and all in the same row
	$('.works article').on('click', '.toggle', function(e){
		e.preventDefault();

		var toggleDescription = function (el) {
			$(el).next('.description').toggle('slow');
			$(el).toggleClass('open');
		};

		var clickedElOffsetTop = $(this).offset().top;

		$.map(allDescriptionLinks, function(link){
			$(link).offset().top === clickedElOffsetTop ? toggleDescription(link) : null;
		});		
	});
});
