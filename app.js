var express = require('express');
var app = express();
var path = require("path");
var exphbs = require('express-handlebars');
var _ = require('lodash');

app.engine('.hbs', exphbs({defaultLayout: 'single', extname: '.hbs'}));
app.set('view engine', '.hbs');

app.use(express.static('public'));

var exampleWork = {
	id: 61160,
	author: 'Anton Testartist',
	title: 'Dies ist ein Beispielwerk',
	image: {
		small: 'https://files.artbutler.com/image/33/w_300_h_300/1280c93f722747e3.jpg',
		big: 'https://files.artbutler.com/image/33/w_2000_h_2000/1280c93f722747e3.jpg'
	},
	description: {
		time: '2015',
		technique: 'Acryl, Sprühfarbe und Photokopie auf Leinwand',
		dimensions: '50 x 70 cm',
		signatur: 'unsigned',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'
	}
};

var images = [
	'1280c93f722747e3.jpg',
	'13e80fc5fc5f4ff6.JPG',
	'fca5b68bebc6422b.jpg',
	'de8f53c91eb34f51.jpg',
	'c1e7275743c34255.jpg',
	'fca5b68bebc6422b.jpg',
	'78c2ab61ad65496b.jpg'
];

var worksData = [];

_.each(images, function(img){
	var work = _.cloneDeep(exampleWork);
	work.image = {
		small: 'https://files.artbutler.com/image/33/w_300_h_300/' + img,
		big: 'https://files.artbutler.com/image/33/w_2000_h_2000/' + img
	}

	worksData.push(work);
});

app.get('/', function (req, res) {
	res.render('index', {
		works: worksData
	});
});

app.get('/stripjs', function (req, res) {
	res.render('index', {
		works: worksData,
		strip: true
	});
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
